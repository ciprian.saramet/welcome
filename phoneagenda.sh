#!/bin/bash


function add() {
 echo
       while true
       do 
       echo "To add a record in the phone agenda please enter the information in the following"
       echo "format: FirstName LastName,Email,Phone"
       echo "Example: Cip Saramet,ciprian.saramet@gmail.com,0722222222"
       echo "To exit, enter 'x' ."
        read aInput
          if [ "$aInput" == "x" ]
          then
          break
          fi
           Name="$(echo $aInput | awk -F "," '{print $1}')"
           echo -e "Name entered is \e[1;31m${Name}\e[0m"
           Email="$(echo $aInput | awk -F "," '{print $2}')"
           echo -e "Email address entered is \e[1;32m${Email}\e[0m"
           Phone="$(echo $aInput | awk -F "," '{print $3}')"
           echo -e "Phone number entered is \e[1;35m${Phone}\e[0m"
           String="$(grep "^([a-zA-Z]{3,30}+[ ]+[a-zA-Z]{3,30})$+[,]+^([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4})$+[,]^([0-9]{10})$" addressbook.csv)"
           Search="$(grep "${Name}" addressbook.csv)"
                if [[ ${Name} =~ ^([a-zA-Z]{3,30}+[ ]+[a-zA-Z]{3,30})$ ]]; then
                 if [ -z "${Search}" ]; then
                  if [[ ${Email} =~ ^([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4})$ ]]; then
                   if [[ ${Phone} =~ ^([0-9]{10})$ ]]; then
                     echo
                     echo $aInput >> addressbook.csv
                     echo "The entry was added to your address book."
                     echo "${Name},${Email},${Phone}"
                    else
                      echo "Phone entry is invalid.Please try again in the 0xxxxxxxxx" 
                   fi
                   else
                     echo "Email entry is invalid.Please try again in the name@domain.xx format"
                  fi
                 else
                  echo "Duplicate entry found:"
                  echo \"${Search}\"
                  echo "Use mod function to edit duplicate entries"
                           break
                 fi
                else
                  echo "Name entry is invalid.Please try again in the FirstName LastName format."
                fi 
        echo
        done
 }
 
function sea() {
       echo
       while true
       do
           echo "Enter a search string - a name, email or phone number"
           echo "To exit search enter 'x'"
           read sInput
             if [[ $sInput == 'x' ]]
                    then
                    break
             else
              grep -n "$sInput" addressbook.csv | cut -d: -f2
              echo
             fi
        done
}

function mod() {
       echo
       while true
       do
           echo "To edit a record enter any name in the format of"
           echo "FirstName Lastname or enter any email adress"
           echo "When you are done editing enter 'x' to quit"
           read mInput
             if [[ $mInput == 'x' ]]
                    then
                    break
            else 
             grep -n "$mInput" addressbook.csv
             echo
               if [ -z $? ]
                    then
                    break
               else
                 search=$(grep "$mInput" addressbook.csv | cut -d : -f1,2,3)
                 line=$(grep -n "$mInput" addressbook.csv | cut -c1)
                 echo "Please select the entry you wish to modify in numerical format"
                 echo "ex: 2"
                 read entry
                 echo
                 if [ "$entry" == "$line" ]
                   then
                    read -p "Enter the modified string >" string
                    sed -i "s/${search}/$string/" addressbook.csv
                   else
                    echo "Invalid entry specified.Please select entry number you wish to modify"
                    echo
                 fi
               fi       
             fi
       done
}                        

function lst() {
      echo 
      if [ 0 -lt 1 ]
       then
            echo "Listing phone agenda..."
            agenda=$(cat -n addressbook.csv)
            echo "$agenda"
      fi                    
}

function rem() {
       echo
       while true
       do
           echo "To remove a record enter any name in the format of"
           echo "FirstName Lastname or enter any email adress"
           echo "When you are done enter 'x' to quit"
           read rInput
             if [[ $rInput == 'x' ]]
                    then
                    break
            else 
             grep -n "$rInput" addressbook.csv
             echo
               if [ -z $? ]
                    then
                    break
               else
                 search=$(grep "$rInput" addressbook.csv | cut -d : -f1,2,3)
                 line=$(grep -n "$rInput" addressbook.csv | cut -c1)
                 echo "Please select the entry you wish to remove in numerical format"
                 echo "ex: 2"
                 read entry
                 echo
                 if [ "$entry" == "$line" ]
                   then
                    sed -i "/${search}/d" addressbook.csv
                   else
                    echo "Invalid entry specified.Please select entry number you wish to remove"
                    echo
                 fi
               fi       
             fi
       done
}

$@
