#!/bin/bash
declare -a x=(0 1 2 3 4 5 6 7 8 9 a b c d e f g h j i k l m n o p q r s t u v w x y z)
url="http://exercise-02"
pasi=4
pasj=3
pask=2
pasl=1
pasm=0

for ((i=0;i<${#x[@]};++i))
  do
     urli="$url/${x[i]}"
     distancei=$(curl -s -G $urli | grep Distance | sed 's/<!--//g' | awk '{print$2}')
     echo "$urli has a distance of $distancei"
     if [ $distancei -lt $pasi ]
        then
           for ((j=0;j<${#x[@]};++j))
             do
                urlj="$urli/${x[$j]}"
                distancej=$(curl -s -G $urlj | grep Distance | sed 's/<!--//g' | awk '{print$2}')
                echo "$urlj has a distance of $distancej"
                if [ $distancej -lt $pasj ]
                   then
                      for ((k=0;k<${#x[@]};++k))
                        do
                           urlk="$urlj/${x[$k]}"
                           distancek=$(curl -s -G $urlk | grep Distance | sed 's/<!--//g' | awk '{print$2}')
                           echo "$urlk has a distance of $distancek"
                           if [ $distancek -lt $pask ]
                              then
                              for ((l=0;l<${#x[@]};++l))
                                do
                                   urll="$urlk/${x[$l]}"
                                   distancel=$(curl -s -G $urll | grep Distance | sed 's/<!--//g' | awk '{print$2}')
                                   echo "$urll has a distance of $distancel"
                                   if [ $distancel -lt $pasl ]
                                   then
                                   for ((m=0;m<${#x[@]};++m))
                                     do
                                        urlm="$urll/${x[$m]}"
                                        distancem=$(curl -s -G $urlm | grep Distance | sed 's/<--//g' | awk '{print$2}')
                                        if [[ $distancem == 0 ]]
                                        then
                                        break
                                        else
                                         echo "$urlm has a distance of $distancem"
                                        fi
                                      done
                                   elif [[ $distancel == 0 ]]
                                   then
                                   break
                                   echo "$urll has a distance of $distancel"
                                   flagl=$(curl -s -G $urll 2>dev/null | grep Flag | awk -F":" '{print$2}')
                                   echo $flag
                                   fi
                                done
                            fi
                          done
                         fi
                       done
         fi
 done

